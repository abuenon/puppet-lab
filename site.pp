# puppet module install puppetlabs-apache --modulepath $puppetmodules
# puppet module install puppetlabs-docker --modulepath $puppetmodules
node 'puppetclient.ec2.internal' {
 #include apache

 class { 'apache':
  default_vhost => false,
 }

 apache::vhost { 'vhost.example.com':
  port    => 80,
  docroot => '/var/www/vhost',
 }

 file { '/var/www/vhost/index.html':
  ensure => file,
  content => "Teste aula puppet",
  require => Class['apache'],
 }

 #### Instalando Docker
 #include 'docker'

 class { 'docker': }

 docker::run { 'nginx':
  image   => 'nginx:latest',
  ports   => ['8080:80'],
  require => Class['docker'],
 }
}